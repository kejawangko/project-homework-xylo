<?php

namespace Database\Seeders;

use App\Http\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'role'           => 'admin',
            'name'           => 'Admin Xylo',
            'email'          => 'admin.xylo@gmail.com',
            'password'       => Hash::make('admin')
        ]);

        User::create([
            'role'           => 'agent',
            'name'           => 'Agent Xylo',
            'email'          => 'agent.xylo@gmail.com',
            'password'       => Hash::make('agent')
        ]);
    }
}
