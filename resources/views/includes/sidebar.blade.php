<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/') }}">
        <div class="sidebar-brand-text mx-3">Admin Panel</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('/') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    @if(Auth::user()->role === 'admin')
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Master Data
    </div>

    <li class="nav-item {{ (Request::is('admin-users') || Request::is('admin-users/*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('admin-users') }}">
            <i class="fas fa-fw fa-users"></i>
            <span>Admin User</span>
        </a>
    </li>

    <li class="nav-item {{ (Request::is('customers') || Request::is('customers/*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('customers') }}">
            <i class="fas fa-fw fa-address-book"></i>
            <span>Customer</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <li class="nav-item {{ (Request::is('follow-up-customers') || Request::is('follow-up-customers/*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('follow-up-customers') }}">
            <i class="fas fa-fw fa-users"></i>
            <span>Follow Up Customer</span>
        </a>
    </li>
    @endif
</ul>
<!-- End of Sidebar -->