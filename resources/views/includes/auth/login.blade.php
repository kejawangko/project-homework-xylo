@extends('layouts.admin.app')

@section('content')

<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header text-center">Welcome Back</div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <img src="{{ asset('logo/logo.png') }}" alt="" width="75%;">
                </div>
            </div>
            <form method="POST" action="{{ url('login-admin') }}" data-parsley-trigger="blur" data-parsley-validate="">
                @csrf
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required="required" autofocus="autofocus" name="email">
                        <label for="inputEmail">Email address</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="required" name="password">
                        <label for="inputPassword">Password</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-main btn-block">Login</button>
            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="register.html">Register an Account</a>
                <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div>
        </div>
    </div>
</div>
@stop