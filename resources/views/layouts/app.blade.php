<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Xylo</title>

  <!-- Custom fonts for this template-->
  <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ asset('css/sb-admin-2.css') }}" rel="stylesheet">
  <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">

</head>

<body @guest class="bg-gradient-primary" @else id="page-top" @endif>

  @yield('content')

  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('js/sb-admin-2.js') }}"></script>
  <script>
    //function number format
    function convertToRupiah(angka) {
        var rupiah = '';        
        var angkarev = angka.toString().split("").reverse().join("");
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
        return rupiah.split('',rupiah.length-1).reverse().join('');
    }
    function convertToAngka(rupiah){
        if(rupiah == ""){
            return "";
        }
        else{
            return parseInt(rupiah.replace(/,*&[a-z][A-Z]|[^0-9]/g, ''), 10);
        }
    }

    function onlyNumeric(value) {
      if(value == ""){
            return "";
        }
        else{
            return value.replace(/,*&[a-z][A-Z]|[^0-9]/g, '');
        }
    }
    //end of number format

    // Call the dataTables jQuery plugin
    $(document).ready(function() {
      $('.dataTable').DataTable({
        responsive: true,
          language: {
              searchPlaceholder: 'Search...',
              sSearch: '',
              lengthMenu: '_MENU_ items/page',
          },
          order : [],
          "bStateSave": true,
          "fnStateSave": function (oSettings, oData) {
              localStorage.setItem('offersDataTables', JSON.stringify(oData));
          },
          "fnStateLoad": function (oSettings) {
              return JSON.parse(localStorage.getItem('offersDataTables'));
          }
      });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

  </script>
  @yield('script')
</body>

</html>
