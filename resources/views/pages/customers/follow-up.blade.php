@extends('layouts.app')

@section('content')
<div id="wrapper">
    @include('includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('includes.navbar')
            <div class="container-fluid">
                @include('includes.alert')
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Follow Up Customer</h1>
                </div>
                <div class="row py-3 bg-white">
                    <div class="col-lg-12 mb-4">
                        <form action="{{ url('follow-up-customers/store') }}" method="post">
                            @csrf
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Agent</span>
                                </div>
                                <select class="form-control" name="agent" id="agent">
                                    <option disabled @if (old('agent') == null) selected @endif>Pilih agent</option>
                                    @foreach ($agents as $agent)
                                        <option @if (old('agent') == $agent->id) selected @endif value="{{ $agent->id }}">{{ $agent->name }}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Customer</span>
                                </div>
                                <select class="form-control" name="customer" id="customer">
                                    <option disabled @if (old('customer') == null) selected @endif>Pilih customer</option>
                                    @foreach ($customers as $customer)
                                        <option @if (old('customer') == $customer->id) selected @endif value="{{ $customer->id }}">{{ $customer->name }}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12 mb-4">
                        <div class="table-responsive">
                            <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Agent Name</th>
                                        <th>Customer Name</th>
                                        <th>Status</th>
                                        <th width="15%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($items as $key => $item)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td id="datacustomer_name{{ $key+1 }}">{{ $item->agent }}</td>
                                            <td id="datacustomer_email{{ $key+1 }}">{{ $item->customer }}</td>
                                            <td id="datacustomer_status{{ $key+1 }}">{{ ucwords(strtolower($item->status)) }}</td>
                                            <td class="text-center">
                                                <a href="{{ url('follow-up-customers/detail/'.$item->id) }}" class="btn btn-primary btn-detail" style="color: white">
                                                    <span class="fas fa-search" title="update"></span>
                                                </a>
                                                <a href="#" class="btn btn-danger btn-delete" style="color: white" data-id="{{ $item->id }}">
                                                    <span class="fas fa-trash" title="update"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<div class="modal fade" id="delCustomerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Data Assign</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="form-delete-customer" action="#" method="get">
                @csrf
                <div class="modal-body">
                    <strong>Are sure ? Once data deleted there's no turning back .</strong>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button id="btn-conf-del-payment-modal" class="btn btn-primary" type="submit">Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('.btn-delete').on('click', function(e) {
        e.preventDefault();

        var idUser = $(this).attr('data-id');
        var target = $('#delCustomerModal');

        target.find('#form-delete-customer').attr('action', "{{ url('customers/assign-to-agents/delete/') }}/"+idUser)

        target.modal('show');
    })
</script>
@endsection