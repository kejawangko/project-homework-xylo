@extends('layouts.app')

@section('content')
<div id="wrapper">
    @include('includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('includes.navbar')
            <div class="container-fluid">
                @include('includes.alert')
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Follow Up Information</h1>
                </div>
                <div class="row py-3 bg-white">
                    <div class="col-lg-12 mb-4">
                        <form action="#">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Agent</span>
                                </div>
                                <input type="text" name="name" id="name" class="form-control" value="{{ $item->agents->name }}" disabled>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Customer</span>
                                </div>
                                <input type="text" name="name" id="name" class="form-control" value="{{ $item->customers->name }}" disabled>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12 mb-4">
                        <div class="table-responsive">
                            <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Agent</th>
                                        <th>Customer</th>
                                        <th>Description</th>
                                        <th>Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($logs as $key => $log)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $log->agent }}</td>
                                        <td>{{ $log->customer }}</td>
                                        <td>{!! $log->description !!}</td>
                                        <td>{{ date('d F Y H:i', strtotime($log->created_at)) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
@endsection

@section('script')
@endsection