@extends('layouts.app')

@section('content')
<div id="wrapper">
    @include('includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('includes.navbar')
            <div class="container-fluid">
                @include('includes.alert')
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Customer</h1>
                </div>
                <div class="row py-3 bg-white">
                    <div class="col-lg-12 mb-4">
                        <form action="{{ url('customers/store') }}" method="post">
                            @csrf
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Name</span>
                                </div>
                                <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Phone</span>
                                </div>
                                <input type="text" name="phone" id="phone" class="form-control" value="{{ old('phone') }}">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Email</span>
                                </div>
                                <input type="text" name="email" id="email" class="form-control" value="{{ old('email') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12 mb-4">
                        <div class="table-responsive">
                            <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th width="15%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($items as $key => $item)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td id="datacustomer_name{{ $key+1 }}">{{ $item->name }}</td>
                                            <td id="datacustomer_phone{{ $key+1 }}">{{ $item->phone }}</td>
                                            <td id="datacustomer_email{{ $key+1 }}">{{ $item->email }}</td>
                                            <td class="text-center">
                                                <a href="{{ url('customers/detail/'.$item->id) }}" class="btn btn-primary btn-detail" style="color: white">
                                                    <span class="fas fa-search" title="update"></span>
                                                </a>
                                                <a href="#" class="btn btn-warning btn-edit" style="color: white" data-id="{{ $item->id }}">
                                                    <span class="fas fa-edit" title="update"></span>
                                                </a>
                                                <a href="#" class="btn btn-danger btn-delete" style="color: white" data-id="{{ $item->id }}">
                                                    <span class="fas fa-trash" title="update"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<div class="modal fade" id="editCustomerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="edit-customer-form" action="#" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="edit-customer-name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" id="edit-customer-phone" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="edit-customer-email" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="delCustomerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Data Customer</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="form-delete-customer" action="#" method="get">
                @csrf
                <div class="modal-body">
                    <strong>Are sure ? Once data deleted there's no turning back .</strong>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button id="btn-conf-del-payment-modal" class="btn btn-primary" type="submit">Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('.btn-edit').on('click', function(e) {
        e.preventDefault();
        
        var idUser = $(this).attr('data-id');
        var currentTr = $(this).closest('tr');
        var target = $('#editCustomerModal');

        var name = currentTr.find('#datacustomer_name'+idUser).text();
        var email = currentTr.find('#datacustomer_email'+idUser).text();
        var phone = currentTr.find('#datacustomer_phone'+idUser).text();

        target.find('#edit-customer-name').val(name);
        target.find('#edit-customer-email').val(email);
        target.find('#edit-customer-phone').val(phone);
        target.find('#edit-customer-form').attr('action', "{{ url('customers/update') }}/"+idUser)

        target.modal('show');
    });

    $('.btn-delete').on('click', function(e) {
        e.preventDefault();

        var idUser = $(this).attr('data-id');
        var target = $('#delCustomerModal');

        target.find('#form-delete-customer').attr('action', "{{ url('customers/delete') }}/"+idUser)

        target.modal('show');
    })
</script>
@endsection