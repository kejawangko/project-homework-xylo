@extends('layouts.app')

@section('content')
<div id="wrapper">
    @include('includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('includes.navbar')
            <div class="container-fluid">
                @include('includes.alert')
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Customer Assigned</h1>
                </div>
                @if(Auth::user()->role === 'admin')
                    <div class="row mb-5">
                        <div class="col-lg-6">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-center">
                                    <p class="m-0 font-weight-bold text-uppercase text-gray-900" style="font-size: 10pt;">Total Customer</p>
                                </div>
                                <div class="card-body text-center">
                                    <h3>{{ $customers }}</h3>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-center">
                                    <p class="m-0 text-center font-weight-bold text-uppercase text-gray-900" style="font-size: 10pt;">Total Customer Uncontacted</p>
                                </div>
                                <div class="card-body text-center">
                                    <h3>{{ $items->where('status', 'uncontacted')->count() }}</h3>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-center">
                                    <p class="m-0 text-center font-weight-bold text-uppercase text-gray-900" style="font-size: 10pt;">Total Customer Pending</p>
                                </div>
                                <div class="card-body text-center">
                                    <h3>{{ $items->where('status', 'pending')->count() }}</h3>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-center">
                                    <p class="m-0 text-center font-weight-bold text-uppercase text-gray-900" style="font-size: 10pt;">Total Customer Qualified</p>
                                </div>
                                <div class="card-body text-center">
                                    <h3>{{ $items->where('status', 'qualified')->count() }}</h3>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-center">
                                    <p class="m-0 text-center font-weight-bold text-uppercase text-gray-900" style="font-size: 10pt;">Total Customer Lost</p>
                                </div>
                                <div class="card-body text-center">
                                    <h3>{{ $items->where('status', 'lost')->count() }}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row mb-5">
                        <div class="col-lg-12 mb-4">
                            <div class="table-responsive">
                                <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th width="15%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($items as $key => $item)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td id="data_name{{ $key+1 }}">{{ $item->name }}</td>
                                                <td id="data_phone{{ $key+1 }}">{{ $item->phone }}</td>
                                                <td id="data_email{{ $key+1 }}">{{ $item->email }}</td>
                                                <td id="data_status{{ $key+1 }}">{{ $item->status }}</td>
                                                <td class="text-center">
                                                    <a href="#" class="btn btn-warning btn-edit" style="color: white" data-id="{{ $item->id }}">
                                                        <span class="fas fa-edit" title="update"></span>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<div class="modal fade" id="editCustomerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="edit-datafollowup-form" action="#" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="status">Status</label>
                            <select name="status" id="edit-datafollowup-status" class="form-control">
                                <option value="uncontacted">Uncontacted</option>
                                <option value="pending">Pending</option>
                                <option value="qualified">Qualified</option>
                                <option value="lost">Lost</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="edit-datafollowup-name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" id="edit-datafollowup-phone" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="edit-datafollowup-email" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="remarks">Remarks</label>
                            <textarea name="remarks" id="edit-datafollowup-remarks" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('.btn-edit').on('click', function(e) {
        e.preventDefault();
        
        var idData = $(this).attr('data-id');
        var currentTr = $(this).closest('tr');
        var target = $('#editCustomerModal');

        var name = currentTr.find('#data_name'+idData).text();
        var email = currentTr.find('#data_email'+idData).text();
        var phone = currentTr.find('#data_phone'+idData).text();
        var status = currentTr.find('#data_status'+idData).text();

        target.find('#edit-datafollowup-status').val(status.toLowerCase());
        target.find('#edit-datafollowup-name').val(name);
        target.find('#edit-datafollowup-email').val(email);
        target.find('#edit-datafollowup-phone').val(phone);
        target.find('#edit-datafollowup-form').attr('action', "{{ url('follow-up-customers/update') }}/"+idData)

        target.modal('show');
    });
</script>
@endsection