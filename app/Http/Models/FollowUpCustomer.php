<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class FollowUpCustomer extends Model
{
    use SoftDeletes;

    protected $table = "follow_up_customers";
    protected $fillable = [
        'agent_id',
        'customer_id',
        'status',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function agents()
    {
        return $this->belongsTo('App\Http\Models\User', 'agent_id');
    }

    public function customers()
    {
        return $this->belongsTo('App\Http\Models\Customer', 'customer_id');
    }

    public function createdby()
    {
        return $this->belongsTo('App\Http\Models\User', 'created_by');
    }

    public function updatedby()
    {
        return $this->belongsTo('App\Http\Models\User', 'updated_by');
    }

    public function deletedby()
    {
        return $this->belongsTo('App\Http\Models\User', 'deleted_by');
    }
}
