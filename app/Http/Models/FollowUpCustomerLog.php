<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class FollowUpCustomerLog extends Model
{
    const UPDATED_AT = null;

    protected $table = "follow_up_customer_logs";
    protected $fillable = [
        'follow_up_customers_id',
        'description',
        'created_by'
    ];

    public function followupcustomers()
    {
        return $this->belongsTo('App\Http\Models\FollowUpCustomer', 'follow_up_customers_id');
    }

    public function createdby()
    {
        return $this->belongsTo('App\Http\Models\User', 'created_by');
    }
}
