<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    protected $table = "customers";
    protected $fillable = [
        'name',
        'phone',
        'email',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function createdby()
    {
        return $this->belongsTo('App\Http\Models\User', 'created_by');
    }

    public function updatedby()
    {
        return $this->belongsTo('App\Http\Models\User', 'updated_by');
    }

    public function deletedby()
    {
        return $this->belongsTo('App\Http\Models\User', 'deleted_by');
    }
}
