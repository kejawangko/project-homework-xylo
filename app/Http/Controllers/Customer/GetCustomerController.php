<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use Illuminate\Http\Request;

class GetCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $items = Customer::get();

        $arrView = [
            'items' => $items
        ];

        return view('pages.customers.index', $arrView);
    }
}
