<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Models\FollowUpCustomer;
use App\Http\Models\FollowUpCustomerLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostUpdateFollowUpCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request, $id)
    {
        $item = FollowUpCustomer::find($id);
        if (!$item) {
            return redirect(url()->previous())->with('failed', 'Data not found');
        }

        try {
            DB::beginTransaction();

            $data = new FollowUpCustomerLog();
            $data->follow_up_customers_id = $id;
            $data->description = Auth::user()->name;
            if ($item->status !== $request->status) {
                $data->description .= " updated to " . $request->status . " from " . $item->status . "<br>";
            }

            if ($request->remarks) {
                $data->description .= " add remarks : " . $request->remarks;
            }

            $data->created_by = Auth::id();
            $data->save();

            FollowUpCustomer::where('id', $id)->update(['status' => $request->status, 'updated_by' => Auth::id()]);
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect('/')->with('success', 'Data updated');
    }
}
