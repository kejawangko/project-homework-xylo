<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use App\Http\Models\FollowUpCustomer;
use App\Http\Models\FollowUpCustomerLog;
use Illuminate\Http\Request;

class GetDetailHistoryFollowUpCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke($id)
    {
        $item = FollowUpCustomer::find($id);
        $logs = FollowUpCustomerLog::where('follow_up_customer_id', $id)->get();

        $arrView = [
            'item' => $item,
            'logs' => $logs
        ];

        return view('pages.customer.detail', $arrView);
    }
}
