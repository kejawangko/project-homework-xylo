<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use App\Http\Models\FollowUpCustomer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetDeleteCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke($id)
    {
        $user = Auth::user();
        if ($user->role !== 'admin') {
            return redirect('/');
        }

        $item = Customer::find($id);
        if (!$item) {
            return redirect(url()->previous())->with('failed', 'Data not found');
        }

        $isCustomerLost = FollowUpCustomer::where('customer_id', $id)->where('status', 'lost')->exists();
        if ($isCustomerLost) {
            return redirect(url()->previous())->with('failed', 'Customer still on follow up');
        }

        try {
            DB::beginTransaction();

            Customer::where('id', $id)->delete();
            DB::table('customers')->where('id', $id)->update(['deleted_by' => $user->id]);
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect('customers')->with('success', 'Data deleted');
    }
}
