<?php

namespace App\Http\Controllers\Customer;

use App\Helpers\GlobalFunction;
use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostUpdateCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->role !== 'admin') {
            return redirect('/');
        }

        $phone = GlobalFunction::normalizePhoneNumber($request->phone);
        $isPhoneExist = Customer::where('id', '<>', $id)->where('phone', $phone)->exists();
        if ($isPhoneExist) {
            return redirect(url()->previous())->with('failed', 'Phone number exists');
        }

        $isEmailExist = Customer::where('id', '<>', $id)->where('email', $request->email)->exists();
        if ($isEmailExist) {
            return redirect(url()->previous())->with('failed', 'Email exists');
        }

        $item = Customer::find($id);
        if (!$item) {
            return redirect(url()->previous())->with('failed', 'Data not found');
        }
        try {
            DB::beginTransaction();

            $data = [];
            $data['name'] = $request->name;
            $data['phone'] = $request->phone;
            $data['email'] = $request->email;
            $data['updated_by'] = $user->id;

            Customer::where('id', $id)->update($data);
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect('customers')->with('success', 'Data updated');
    }
}
