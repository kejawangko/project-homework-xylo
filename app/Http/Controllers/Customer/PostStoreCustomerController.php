<?php

namespace App\Http\Controllers\Customer;

use App\Helpers\GlobalFunction;
use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostStoreCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request)
    {
        $user = Auth::user();
        if ($user->role !== 'admin') {
            return redirect('/');
        }

        $phone = GlobalFunction::normalizePhoneNumber($request->phone);
        $isPhoneExist = Customer::where('phone', $phone)->exists();
        if ($isPhoneExist) {
            return redirect(url()->previous())->with('failed', 'Phone number exists');
        }

        $isEmailExist = Customer::where('email', $request->email)->exists();
        if ($isEmailExist) {
            return redirect(url()->previous())->with('failed', 'Email exists');
        }
        try {
            DB::beginTransaction();

            $data = new Customer();
            $data->name = $request->name;
            $data->phone = $request->phone;
            $data->email = $request->email;
            $data->created_by = $user->id;
            $data->save();
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect('customers')->with('success', 'Data inserted');
    }
}
