<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use App\Http\Models\FollowUpCustomer;
use App\Http\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostStoreFollowUpCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request)
    {
        $user = Auth::user();
        if ($user->role !== 'admin') {
            return redirect('/');
        }

        $isAgentExist = User::where('id', $request->agent)->exists();
        if (!$isAgentExist) {
            return redirect(url()->previous())->with('failed', 'Agent not found');
        }

        $item = Customer::where('id', $request->customer)->exists();
        if (!$item) {
            return redirect(url()->previous())->with('failed', 'Data not found');
        }

        try {
            DB::beginTransaction();

            $data = new FollowUpCustomer();
            $data->agent_id = $request->agent;
            $data->customer_id = $request->customer;
            $data->created_by = $user->id;
            $data->save();
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect('follow-up-customers')->with('success', 'Data inserted');
    }
}
