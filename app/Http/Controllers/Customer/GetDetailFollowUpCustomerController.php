<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Models\FollowUpCustomer;
use App\Http\Models\FollowUpCustomerLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GetDetailFollowUpCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke($id)
    {
        $user = Auth::user();
        if ($user->role !== 'admin') {
            return redirect('/');
        }

        $item = FollowUpCustomer::find($id);
        if (!$item) {
            return redirect(url()->previous())->with('failed', 'Data not found');
        }

        $logs = FollowUpCustomerLog::select('follow_up_customer_logs.description', 'users.name as agent', 'customers.name as customer', 'follow_up_customer_logs.created_at as created_at')
            ->join('follow_up_customers', 'follow_up_customers.id', '=', 'follow_up_customer_logs.follow_up_customers_id')
            ->join('customers', 'customers.id', '=', 'follow_up_customers.customer_id')
            ->join('users', 'users.id', '=', 'follow_up_customer_logs.created_by')
            ->where('follow_up_customers_id', $id)->get();

        $arrView = [
            'item' => $item,
            'logs' => $logs
        ];

        return view('pages.customers.follow-up-log', $arrView);
    }
}
