<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use App\Http\Models\FollowUpCustomer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetDeleteAssignToAgentCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke($id)
    {
        $user = Auth::user();
        if ($user->role !== 'admin') {
            return redirect('/');
        }

        $item = FollowUpCustomer::find($id);
        if (!$item) {
            return redirect(url()->previous())->with('failed', 'Data not found');
        }

        $customer = Customer::find($item->customer_id);
        if (!$customer) {
            return redirect(url()->previous())->with('failed', 'Customer not found');
        }

        try {
            DB::beginTransaction();

            FollowUpCustomer::where('id', $id)->delete();
            DB::table('follow_up_customers')->where('id', $id)->update(['deleted_by' => $user->id]);
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect('customers/detail/' . $customer->id)->with('success', 'Data deleted');
    }
}
