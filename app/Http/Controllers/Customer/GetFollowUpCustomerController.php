<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use App\Http\Models\FollowUpCustomer;
use App\Http\Models\User;
use Illuminate\Http\Request;

class GetFollowUpCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $agents = User::where('role', 'agent')->get();
        $customers = Customer::get();
        $items = FollowUpCustomer::select('follow_up_customers.id', 'users.name as agent', 'customers.name as customer', 'follow_up_customers.status')
            ->join('users', 'users.id', '=', 'follow_up_customers.agent_id')
            ->join('customers', 'customers.id', '=', 'follow_up_customers.customer_id')
            ->get();

        $arrView = [
            'agents'    => $agents,
            'customers' => $customers,
            'items'     => $items
        ];

        return view('pages.customers.follow-up', $arrView);
    }
}
