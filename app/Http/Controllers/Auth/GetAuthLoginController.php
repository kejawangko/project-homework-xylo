<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

class GetAuthLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function __invoke()
    {
        return view('pages.auth.login');
    }
}
