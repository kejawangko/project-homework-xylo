<?php

namespace App\Http\Controllers\AdminUser;

use App\Http\Controllers\Controller;
use App\Http\Models\User;
use Illuminate\Support\Facades\Auth;

class GetAdminUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $user = Auth::user();
        if ($user->role !== 'admin') {
            return redirect('/');
        }

        $items = User::get();

        $arrView = [
            'items' => $items
        ];

        return view('pages.admin-users.index', $arrView);
    }
}
