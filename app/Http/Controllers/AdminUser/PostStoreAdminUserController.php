<?php

namespace App\Http\Controllers\AdminUser;

use App\Http\Controllers\Controller;
use App\Http\Models\User;
use App\Mail\NewUserMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class PostStoreAdminUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request)
    {
        $user = Auth::user();
        if ($user->role !== 'admin') {
            return redirect('/');
        }

        $isEmailExist = User::where('email', $request->email)->exists();
        if ($isEmailExist) {
            return redirect(url()->previous())->with('failed', 'Email exists');
        }
        try {
            DB::beginTransaction();

            $data = new User();
            $data->role = $request->role;
            $data->name = $request->name;
            $data->email = $request->email;
            $data->password = Hash::make('123456');
            $data->save();
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect(url()->previous())->with('success', 'Data inserted');
    }
}
