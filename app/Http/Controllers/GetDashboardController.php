<?php

namespace App\Http\Controllers;

use App\Http\Models\Customer;
use App\Http\Models\FollowUpCustomer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GetDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $user = Auth::user();
        $arrView = [];
        $items = null;
        if ($user->role === 'admin') {
            $customers = Customer::count();

            $items = new FollowUpCustomer();
            $arrView['customers'] = $customers;
        } else {
            $items = FollowUpCustomer::select('follow_up_customers.id', 'customers.name', 'customers.email', 'customers.phone', 'status')->join('customers', 'customers.id', '=', 'follow_up_customers.customer_id')->where('agent_id', $user->id)->get();
        }

        $arrView['items'] = $items;

        return view('pages.index', $arrView);
    }
}
