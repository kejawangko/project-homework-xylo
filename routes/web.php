<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\GetAuthLoginController')->name('login');
Route::post('login', 'Auth\PostAuthLoginController');
Route::get('logout', function () {
    Auth::logout();
    return redirect('/');
});

Route::get('/', 'GetDashboardController');

Route::prefix('admin-users')->group(function () {
    Route::get('/', 'AdminUser\GetAdminUserController')->name('admin-users');
    Route::post('store', 'AdminUser\PostStoreAdminUserController')->name('admin-users.store');
    Route::post('update/{id}', 'AdminUser\PostUpdateAdminUserController')->name('admin-users.update');
});

Route::prefix('customers')->group(function () {
    Route::get('/', 'Customer\GetCustomerController')->name('customers');
    Route::post('store', 'Customer\PostStoreCustomerController')->name('customers.store');
    Route::post('update/{id}', 'Customer\PostUpdateCustomerController')->name('customers.update');
    Route::get('delete/{id}', 'Customer\GetDeleteCustomerController')->name('customers.delete');

    Route::get('detail/{id}', 'Customer\GetDetailCustomerController')->name('customers.detail');
    Route::get('assign-to-agents/delete/{id}', 'Customer\GetDeleteAssignToAgentCustomerController')->name('customers.assign-to-agents.delete');
});

Route::prefix('follow-up-customers')->group(function () {
    Route::get('/', 'Customer\GetFollowUpCustomerController')->name('follow-up-customers');
    Route::post('store', 'Customer\PostStoreFollowUpCustomerController')->name('follow-up-customers.store');
    Route::get('detail/{id}', 'Customer\GetDetailFollowUpCustomerController')->name('follow-up-customers.detail');
    Route::post('update/{id}', 'Customer\PostUpdateFollowUpCustomerController')->name('follow-up-customers.update');
});
